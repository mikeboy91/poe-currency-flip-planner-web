import sys

import asyncio
import uvicorn
import json

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from pydantic import BaseModel, Field

sys.path.append('poecfp')

from src.pathfinder import PathFinder
from src.core.backends.poeofficial import PoeOfficial
from src.trading import ItemList
from src.config.user_config import UserConfig
from src.commons import load_excluded_traders

class FlipRequest(BaseModel):
    league: str
    transactions: int = Field(..., ge=2, le=3)

config = {"fullbulk": False}
user_config = UserConfig.from_file('./poecfp/config/config.default.json')
excluded_traders = load_excluded_traders()
item_pairs = user_config.get_item_pairs()

app = FastAPI()

origins = [
    "http://localhost:4300",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.post("/api/find-currency-flips")
async def find_currency_flips(request: FlipRequest):
    p = PathFinder(request.league, item_pairs, user_config, excluded_traders)
    await p.run(request.transactions)
    return json.JSONDecoder().decode(p.prepickle())

@app.get("/api/testrun")
async def testrun():
    with open('dummy.json') as json_file:
        data = json.load(json_file)
        return data
