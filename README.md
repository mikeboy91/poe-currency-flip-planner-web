# PoE-Currency-Flip-Planner-Web

A wrapper for poe-currency-flip-planner

![Screenshot](./screenshots/screen1.png)

# Setup

A fork of poe-currency-flip-planner is imported via git submodules. Clone with `git clone --recursive https://gitlab.com/mikeboy91/poe-currency-flip-planner-web`. You only need Docker and docker-compose to run everything.

## Local Dev Setup

1. go to `docker-dev-server`
2. run `docker-compose build`
3. run `docker-compose up --remove-orphans`
4. run `./init-client.sh`
5. restart your compose stack

Client runs on http://localhost:4300

Server runs on http://localhost:4301

## Production Setup

1. `docker-compose build`
2. `docker-compose up --remove-orphans`

Everything runs over a reverse proxy in http://localhost:4300

# Run Tests

Run E2E tests (cypress) in headless mode: `docker exec poecfp-node-shell npm run ng e2e`
