before(() => {
  cy.visit('/');
});

beforeEach(() => {
  cy.get('button[type=submit]').as('findFlipsBtn');
});

describe('find flips', () => {

  it('loads page', () => {
    cy.contains('PoE Currency-Flip-Planner Web');
  });

  it('finds some currency flips', () => {
    cy.get('button[type=submit]').as('findFlipsBtn');

    cy.server();
    cy.route({
      method: 'POST',
      url: '**/find-currency-flips',
      response: 'fixture:flips.json',
      delay: 3000
    }).as('findFlips');

    cy.get('@findFlipsBtn')
      .should('be.enabled')
      .screenshot()
      .click()
      .should('be.disabled')
      .screenshot()
      .wait('@findFlips')
      .get('@findFlipsBtn')
      .should('be.enabled')
      .screenshot();
  });

});
