import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from './../environments/environment';
import { FlipService } from './flip.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit {
  title = 'PoE Currency Flip Planner';
  results = [];
  currencies = [];
  availableResults = [];
  availableCurrencies = [];
  isLoadingFlips = false;
  availableLeagues = ['Heist', 'Standard', 'Hardcore'];

  searchParamsForm = new FormGroup({
    league: new FormControl(this.availableLeagues[0]),
    transactions: new FormControl(2, [Validators.min(2), Validators.max(3)]),
  });

  constructor(
    private http: HttpClient,
    private flipService: FlipService,
    ) { }

  ngOnInit(): void {}

  findFlips(): any {
    this.isLoadingFlips = true;
    this.flipService.findFlippableCurrency(this.searchParamsForm.value.league, this.searchParamsForm.value.transactions).subscribe((result: any) => {
      this.results = result.results;
      this.currencies = Object.keys(this.results);
      this.currencies.forEach(c => {
        if (this.results[c].length !== 0) {
          this.availableResults[c] = this.results[c];
        }
      });

      this.availableCurrencies = Object.keys(this.availableResults);

      this.isLoadingFlips = false;
    }, () => {
      console.error('error');
      this.isLoadingFlips = false;
    });
  }

  runDemo(): any {
    this.http.get('/api/testrun').subscribe((result: any) => {
      this.results = result.results;
      this.currencies = Object.keys(this.results);
      this.currencies.forEach(c => {
        if (this.results[c].length !== 0) {
          this.availableResults[c] = this.results[c];
        }
      });

      this.availableCurrencies = Object.keys(this.availableResults);

      console.log(this.availableResults);
    });
  }

  isProduction(): boolean {
    return environment.production;
  }
}
