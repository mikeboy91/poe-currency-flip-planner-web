export interface FlipResponse {
  timestamp: string,
  league: string,
  item_pairs: any[],
  offers: any[],
  graph: any,
  results: FlipResult,
}

export interface FlipResult {
  [key: string]: CurrencyFlip[],
}

export interface CurrencyFlip {
  from: string,
  to: string,
  starting: number,
  ending: number,
  winnings: number,
  transactions: Transaction[],
}

export interface Transaction {
  league: string,
  have: string,
  want: string,
  contact_ign: string,
  conversion_rate: string,
  stock: number,
  received: number,
  paid: number,
}
