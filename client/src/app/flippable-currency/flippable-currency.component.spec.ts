import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlippableCurrencyComponent } from './flippable-currency.component';

describe('FlippableCurrencyComponent', () => {
  let component: FlippableCurrencyComponent;
  let fixture: ComponentFixture<FlippableCurrencyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlippableCurrencyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlippableCurrencyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
