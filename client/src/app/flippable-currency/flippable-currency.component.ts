import { Component, OnInit, Input } from '@angular/core';
import { faCopy, faArrowRight } from '@fortawesome/free-solid-svg-icons';
import { CurrencyFlip, Transaction } from './views';

@Component({
  selector: 'app-flippable-currency',
  templateUrl: './flippable-currency.component.html',
  styleUrls: ['./flippable-currency.component.sass']
})
export class FlippableCurrencyComponent implements OnInit {
  @Input() flips: CurrencyFlip[];
  faCopy = faCopy;
  faArrowRight = faArrowRight;

  constructor() { }

  ngOnInit(): void {
  }

  buildTransactionMessage(transaction: Transaction): string {
    return `@${transaction.contact_ign} Hi, I'd like to buy your ${transaction.received} ${transaction.want} for my ${transaction.paid} ${transaction.have} in ${transaction.league}.`;
  }

}
