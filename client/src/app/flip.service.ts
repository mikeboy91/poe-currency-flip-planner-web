import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FlipService {

  constructor(private http: HttpClient) { }

  findFlippableCurrency(league: string, transactions: number): Observable<any> {
    return this.http.post('/api/find-currency-flips', {league: league, transactions: transactions});
  }
}
